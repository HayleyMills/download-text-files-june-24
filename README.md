## Export text files from archivist

- Question
    - QV (Question Variables) qv.txt, rename prefix.qvmapping.txt
    - TQ (Topic Questions) tq.txt, rename prefix.tqlinking.txt

- Datasets
    - TV: tv.txt, rename: prefix.tvlinking.txt
    - DV: dv.txt, rename: prefix.dv.txt

### input
List of prefix: Prefixes_to_export.txt

### output: export_txt directory
- text files, organized by
    - prefix
    - data type

- summary table text_list.csv: 
    - prefix
    - text name
    - text location

